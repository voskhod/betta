# Betta

Real numbers, data science and chaos:
How to fit any dataset with a single parameter
https://arxiv.org/pdf/1904.12320.pdf
