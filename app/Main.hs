-- Copyright (C) -- 2021
-- This file is part of Betta.
--
--  Betta is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Betta is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Betta.  If not, see <https://www.gnu.org/licenses/>.

module Main where

import Lib
import Data.Number.CReal
import System.Environment

main = do
      args <- getArgs
      content <- readFile (args !! 0)
      let linesOfFiles = lines content
      let raw_data = (map read linesOfFiles) :: [Double]
      putStrLn (showCReal 150 (computeAlpha raw_data))
