-- Copyright (C) -- 2021
-- This file is part of Betta.
--
--  Betta is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  Betta is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with Betta.  If not, see <https://www.gnu.org/licenses/>.

module Lib
    ( computeAlpha
    ) where

import Data.Fixed
import Data.Char
import Data.Number.CReal

tau = 15

dyadic :: Double -> Double
dyadic x = (2 * x) `mod'` 1

decimalToBin :: Double -> String
decimalToBin dec =
  snd (
    foldl
      (\acc _ -> (dyadic (fst acc), (snd acc ++ [(if fst acc < 0.5 then '0' else '1')])))
      (dec, "")
      [0..tau]
  )

binToDecimal :: [Char] -> CReal
binToDecimal bin =
  foldl
    (\acc val -> acc + (fromIntegral (digitToInt (fst val)) / (2 ** (snd val + 1))))
    (0.0 :: CReal)
    (zip bin [0..])

phiInv :: Double -> Double
phiInv x = (asin (sqrt x)) / (2 * pi)

decimalToBin_phiInv :: Double -> String
decimalToBin_phiInv x = decimalToBin (phiInv x)

phi :: CReal -> CReal
phi alpha = sin (2 * pi * alpha) ** 2

computeAlpha :: [Double] -> CReal
computeAlpha xs = phi (binToDecimal (concat (map decimalToBin_phiInv xs)))






